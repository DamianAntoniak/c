/*
    by: Damian Antoniak aka. Wax
    skype: fty997
    date: 01-21-2014r.

16znakow z 36elementowego zbioru ( 16^36 = lolz :D)
*/

#include <iostream>
#include <ctime>
#include <string>
#include <fstream>
#include "Windows.h"

#define MAX_ 10000

void gen(int, std::string *, char *, int);
int sprawdz(int, std::string *);

int main()
{
    int x;
    char tab[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    std::string warjacja[MAX_];
    SetConsoleTitleA("Generator by Wax");
    srand(time(NULL));


    for(int j = 1; j < MAX_; j++)
    {
        gen(j, warjacja, tab, sizeof(tab));
        x = sprawdz(j, warjacja);
        while(x == 1 || x == 2)
        {
            warjacja[j].clear();
            gen(j, warjacja, tab, sizeof(tab));
            x = sprawdz(j, warjacja);
        }
        std::cout << j << ": kod: " << warjacja[j] << std::endl;
    }
    return 0;
}

int sprawdz(int in, std::string * wax)
{
    for(int p = 0; p < in; p++) if(wax[p] == wax[in]) return 1;

    std::string waxio;  int found;
    std::fstream zw("keys.txt", std::ios::in);

    while(std::getline(zw, waxio))
    {
        found = waxio.find(wax[in]);
        if(found != std::string::npos)
        {
            waxio.clear();
            zw.close();
            return 2;
            break;
        }
        waxio.clear();
        zw.close();
    }

    char data[64];
    time_t czas;
    time(& czas);
    tm czasTM = * localtime(& czas);
    strftime(data, sizeof(data), "%c", & czasTM);

    std::fstream zw2("keys.txt", std::ios::out | std::ios::app);
    zw2 << data <<": " << wax[in] << "\n";
    zw2.close();
    return 0;
}

void gen(int index, std::string * w, char * t, int s)
{
    for(int i = 1; i < 21; i++)
    {
        w[index] += t[rand() % (s - 1)];
        if(!(i % 4) && i < 20)  w[index] += '-';
    }
}
