#include <iostream>
#include "windows.h"
#include <cstdlib>

int main()
{
    int t[4];
    std::cout << "Podaj cztery dowolne liczby:\n";
    std::cin >> t[0] >> t[1] >> t[2] >> t[3];

    int maxx = t[0];
    int minn = t[0];
    for(int i = 0; i < sizeof(t) / sizeof(int); i++)
    {
        if(t[i] > maxx) maxx = t[i];
        if(t[i] < minn) minn = t[i];
    }

    std::cout << "Najmniejsza liczba w tablicy to: " << minn;
    std::cout << "\n\nPo sortowaniu: ";
    for(int a = 0; a < 4; a++) for(int j = 0; j < 4; j++) if(t[j] > t[j + 1]) std::swap(t[j+1], t[j]);

    for(int i = 0; i < 4; i++) std::cout << t[i] << ", ";
    return 0;
}
