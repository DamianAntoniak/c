#include <iostream>
#include "Windows.h"
#include <fstream>
#include <ctime>

#define TAB 0x09
#define LCTRL 0xA2

int main ()//VK_TAB, VK_RETURN
{
    std::fstream file("C:\\Users\\damia_000\\Documents\\gen_V2\\keys.txt", std::ios::in);

    if(!file.is_open()) std::cout << "nie otworzono pliku\n";

    std::string str;
    Sleep(5000);

    while(std::getline(file, str))
    {
        std::cout << "Przed cieciem:" << str << "\n";
        str.erase(0, 19);
        std::cout << "Po cieciu:" << str << "\n";

        char data[64];
        time_t czas;
        time(& czas);
        tm czasTM = * localtime(& czas);
        strftime(data, sizeof(data), "%c", & czasTM);

        std::fstream file2("spr_keys.txt", std::ios::out | std::ios::app);
        file2 << data <<": " << str << "\n";
        file2.close();

        BOOL bRes = OpenClipboard(NULL);

        EmptyClipboard();
        HGLOBAL hNewClipboardData =::GlobalAlloc(GMEM_MOVEABLE, sizeof(char) *(str.size() + 1));

        if(hNewClipboardData)
        {
            LPSTR strNewClipboardText =(LPSTR) GlobalLock(hNewClipboardData);
            ::memcpy(strNewClipboardText, str.c_str(), sizeof(char) *(str.size() + 1));
            GlobalUnlock(hNewClipboardData);
            ::SetClipboardData(CF_TEXT, hNewClipboardData);
            CloseClipboard();
        }

        for(int i = 0; i < 2; i++)
        {
            keybd_event(TAB, 0, 0, 0);
            keybd_event(TAB, 0, KEYEVENTF_KEYUP, 0);
            Sleep(250);
        }

        keybd_event(LCTRL, 0, 0, 0);
        keybd_event(VkKeyScan('V'), 0, 0, 0);

        keybd_event(LCTRL, 0, KEYEVENTF_KEYUP, 0);
        keybd_event(VkKeyScan('V'), 0, KEYEVENTF_KEYUP, 0);

        keybd_event(0x0D, 0, 0, 0);
        keybd_event(0x0D, 0, KEYEVENTF_KEYUP, 0);
        Sleep(2500);
    }
    return 0;
}
