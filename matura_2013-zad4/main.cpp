/*
    By Wax @ 22.11.2013
        e-mail: damianapl17pl@gmail.com
        skype: fty997

        Zad 4 Matura 2013.
*/
#include <iostream>
#include <fstream>
#include <string>

int main()
{
    int t = 0, p = 0, zera = 0, same_zera = 0, same_jedynki = 0, k[16], rowno = 0, ww[1000], www[1000];
    std::fstream file;
    std::ofstream outfile;

    outfile.open("zadanie4.txt");
    file.open("napisy.txt");

    if(!file.is_open()) std::cout << "Nie otworzono pliku\n";
    else std::cout << "Otworzono plik\n";

    std::string w[1000];

    while(std::getline(file, w[t]))
    {
        if(!(w[t].size() % 2)) p++;
        for(int i = 0; i < w[t].size(); i++) if(w[t][i] == '0') zera++;

        if(zera > 0) ww[t] = zera;
        www[t] = w[t].size() - zera;

        if(www[t] * 2 == w[t].size()) rowno++;

        if(ww[t] > 0 && !www[t]) same_zera++;
        else if(!ww[t] && www[t] > 0) same_jedynki++;

        k[w[t].size() - 1]++;

        zera = 0;
        t++;
    }
    file.close();

    outfile << "A) " << p << "\nB) " << rowno << "\nC) same jedynki: " << same_jedynki << " same zera " << same_zera << "\nD) ";
    for(int j = 0; j < 16; j++) outfile << "\nciagow " << j << "-znakowych jest: " << k[j];
    outfile.close();

    std::cin.get();
    return 0;
}
