#include <iostream>


int main()
{
    int a, b, k = 0;
    std::cout << "Poaj dzielna i dzielnik:";
    std::cin >> a >> b;
    if(a == 0) std::cout << "Nie wolno dzielic przez zero!\n";
    else
    {
        while(a >= b)
        {
            a = a-b;
            k++;
        }
        std::cout << "a/b wynosi: " << k;
    }
    return 0;
}
