#include <iostream>
#include "windows.h"
#include <cstdlib>

using namespace std;
int main()
{
    system("chcp 1250");
    system("cls");
    setlocale (LC_ALL, "");
    int t[] = {8, 2, 1, 3};
    int a, b;
    int maxx, minn;
    cout << "Tablica wygląda tak: ";
    for(int i = 0; i < sizeof(t) / sizeof(int); i++) cout << t[i] << ", ";

    cout << "\nNajwiększa liczba w tablicy: ";

    maxx = t[0];
    minn = t[0];
    for(int i = 0; i < sizeof(t) / sizeof(int); i++)
    {
        if(t[i] > maxx) maxx = t[i];
        if(t[i] < minn) minn = t[i];
    }
    cout << maxx << endl << "Najmniejsza liczba w tablicy: " << minn << endl;
    cout << "Po sortowaniu: ";
    for(int i = 0; i < 4; i++) for(int j = 0; j < 4; j++) if(t[j] > t[j + 1]) swap(t[j+1], t[j]);

    for(int i = 0; i < 4; i++) cout << t[i] << ", ";
    cout << endl;
    return 0;
}
