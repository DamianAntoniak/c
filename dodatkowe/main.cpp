/*
    by Wax aka Takisobiekoles @ 20.01.2012r
        Skype: fty997
        e-mail: damianapl17pl@gmail.com
*/
#include <iostream>
#include<stdio.h>

using namespace std;

int main()
{
    int a, b;
    cout << "Podaj liczb A: ";  cin >> a;
    cout << "\nPodaj liczb B: ";  cin >> b;

    cout << endl << "Operatory arytmetyczne" << endl;
    printf("SUMA: %d + %d = %d\n", a, b, a + b);
    printf("ROZNICA: %d - %d = %d\n", a, b, a - b);
    printf("MNOZENIE: %d * %d = %d\n", a, b, a * b);
    printf("DZIELENIE: %d / %d = %d\n", a, b, a % b);
    printf("DZIELENIE(reszta): %d %% %d = %d\n", a, b, a % b);

    cout << endl << "Operatory arytmetyczne" << endl;
    printf("AND: ((%d + %d) && (%d - %d)) = %d\n", a, a, b, b, ((a + a) && (b - b)));
    printf("OR: (%d = %d || %d = %d) = %d\n", a, a, b, b, (a == a || b == b));
    printf("NOT: !(%d = %d) = %d\n", a, a, !(a == a));
    printf("Mniejsze: %d < %d = %d\n", a, b, a < b);
    printf("Wieksze: %d > %d = %d\n", a, b, a > b);
    printf("Mniejsze rowne: %d <= %d = %d\n", a, b, a <= b);
    printf("Wieksze rowne: %d >= %d = %d\n", a, b, a >= b);
    printf("Rowne: (%d == %d) = %d\n", a, b, a == b);
    printf("Rozne: (%d != %d) = %d\n", a, b, a != b);

    return 0;
}
