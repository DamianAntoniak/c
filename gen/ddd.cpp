//---------------------------------------------------------------------------
#pragma hdrstop
//---------------------------------------------------------------------------
#pragma argsused

#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <time.h>

//Start funkcji obliczaj¹cej silnie (permutacje).
float Silnia(int wielkosc_zbioru) //Funkcja obliczaj¹ca silnie (permutacje zbioru), w sposób rekurencyjny.
                                //Opis rekurencji: Najpierw jest wywo³ywana funkcja z wielkoœci¹ zbioru, nastêpnie jest ona wywo³ywana z co raz mniejsz¹ wielkoœci¹ zbioru, a¿ do jeden.
                                //Nastêpnie od najmniejszej wielkoœci zbioru równej jeden, funkcja cofa siê (obliczaj¹c kolejne silnie) do wywo³ania funkcji z wielkoœci¹ zbioru podan¹ na pocz¹tku.
{
//Wzór: n * n * n * ... * n = n!
//      1   2   3         n

        if ((wielkosc_zbioru == 1) || (wielkosc_zbioru == 0))
        {
                return (1);
        }
        else
        {
                return ( wielkosc_zbioru * Silnia(wielkosc_zbioru - 1) ); //Wchodzimy w g³¹b, a¿ do jeden.
        }
}

//Start funkcji obliczaj¹cej wariacje z powtórzeniami.
float ObliczWariacjeZPowtorzeniami(int wielkosc_zbioru, int m)
{
//Wzór: n do m (n^m)

        int wynik = wielkosc_zbioru;
        for (int i = 1; i < m; i++)
        {
                wynik = wynik * wielkosc_zbioru;
        }
        return (wynik);
}

void GENEROWANIE_LOSOWYCH_WARIACJI_Z_POWTORZENIAMI_W_LICZBIE_k(int wielkosc_zbioru/*35*/, int wielkosc_podzbioru/*moje 16*/, int k)
//GENEROWANIE k LOSOWYCH WARIACJI wielkosc_podzbioru ELEMENTOWYCH Z POWTÓRZENIAMI ZE ZBIORU wielkosc_zbioru ELEMENTOWEGO.
{
     int tmp[20][1000]; //pamiêæ ju¿ wylosowanych wariacji.

     int podzbior[20]; //podzbiór przechowuj¹cy wygenerowan¹ losowo wariacje z powtórzeniami.
     int i, j, z; //- liczniki pêtli.
     int zmp; //wylosowana pojedyñcza liczba w zakresie od 1 do wielkosc_zbioru
     int powtorka; //zmienna odpowiedzialna za losowanie podzbior, tak d³ugo a¿ bêdzie to w³aœciwa z punktu widzenia matematycznego wariacja wielkosc_podzbioru elementowa z powtórzeniami ze zbioru wielkosc_zbioru elementowego.
     int powtorka2; //powtarzanie generowania wariacji z powtórzeniami, tak d³ugo a¿ bêdzie siê powtarzaæ. Celem jest uzyskanie nie wygenerowanej wczeœniej wariacji z powtórzeniami.
     int ilosc; //zmienna odpowiedzialna za wylosowanie tylko wielkosc_podzbioru elementów do podzbioru podzbior.
     int przecinek; //czy wypisywaæ przecinek.

     //ILOSC LOSOWAÑ - ALGORYTM GENEROWANIA WARIACJI Z POWTÓRZENIAMI.
     for (j = 1; j <= k; j++)
     {
          przecinek = 0;
          ilosc = 0;

          //Losowanie wariacji wielkosc_podzbioru elementowej z powtórzeniami ze zbioru wielkosc_zbioru elementowego.
          powtorka2 = 0;
          while (powtorka2 == 0)
          {
              //Wylosowanie prawid³owej z punktu widzenia matematyki wariacji z powtórzeniami.
              while (ilosc < wielkosc_podzbioru)
              {
                 zmp = rand() % wielkosc_zbioru + 1;
                 ilosc = ilosc + 1;
                 podzbior[ilosc-1] = zmp;
              };

              //Sprawdzenie, czy ta wariacja z powtórzeniami by³a ju¿ wczeœniej wylosowana i jak tak to powtórka losowania.
              powtorka2 = 1;
              for (i = 1; i <= k; i++)
              {
                  if (tmp[0][i-1] == 0)
                  {
                     break;
                  }
                  for (z = 1; z <= wielkosc_podzbioru; z++)
                  {
                      if (tmp[z-1][i-1] == podzbior[z-1])
                      {
                          powtorka2 = 0;
                      }
                      else
                      {
                          powtorka2 = 1;
                          break;
                      }
                  }
                  if (powtorka2 == 0)
                  {
                      for (i = 1; i <= wielkosc_podzbioru; i++)
                      {
                         podzbior[i-1] = 0;
                      }
                      ilosc = 0;
                      break;
                  }
              }
          }
          //Koniec losowania wariacji wielkosc_podzbioru elementowej z powtórzeniami ze zbioru wielkosc_zbioru elementowego.

          printf("\nWariacja z powtorzeniami %d: ", j);

          //Wypisywanie i zapamiêtywanie wariacji z powtórzeniami.
          for (i = 1; i <= wielkosc_podzbioru; i++)
          {
                 printf("%d", podzbior[i-1]);
                 tmp[i-1][j-1] = podzbior[i-1];
                 przecinek = przecinek + 1;
                 if (przecinek < wielkosc_podzbioru)
                 {
                    printf(", ");
                 }
          }
          //Ponowne zerowanie podzbior i przygotowanie do nastêpnego generowania.
          for (i = 1; i <= wielkosc_podzbioru; i++)
          {
                 podzbior[i-1] = 0;
          }
          printf("\n");
     }
}

//START PROGRAMU.
int main(int argc, char* argv[])
{
        int wielkosc_zbioru;
        int m; //m - podzbiory m-elementowe zbioru n-elementowego (wielkoœæ podzbioru - wariacji z powtórzeniami).
        int k; //k - iloœæ losowañ.
        float silnia; //wynik silnia (permutacje). //Zastosowa³em liczbê rzeczywist¹ chocia¿ wynik permutacji jest zawsze ca³kowity, lecz liczby ca³kowite maj¹ zbyt ma³y zakres.
        float wariacje_z_powtorzeniami; //wynik wariacje z powtórzeniami. //Zastosowa³em liczbê rzeczywist¹ chocia¿ wynik wariacji jest zawsze ca³kowity, lecz liczby ca³kowite maj¹ zbyt ma³y zakres.

        printf("Podaj wielkosc zbioru (od 3 do 20): ");
        scanf("%d", &wielkosc_zbioru);

        //Sprawdzenie ograniczeñ na wielkoœæ zbioru.
        if ( (wielkosc_zbioru < 3) || (wielkosc_zbioru > 20) )
        {
                printf("\n\nPodales wielkosc zbioru poza zakresem!\n\n");
                printf("\n\nNacisnij ENTER");

                getche();
                exit (0);
        }

        //Rozpoczêcie oblicznia silni (permutacji).
        silnia = Silnia(wielkosc_zbioru);

        printf("\nPodaj wielkosc podzbioru: "); //Podanie wielkoœci podzbioru.
        scanf("%d", &m);

        if (m > wielkosc_zbioru)
        {
                printf("\nPodales wielkosc podzbioru wieksza niz wielkosc zbioru!\n\n");
                printf("\nNacisnij ENTER");

                getche();
                exit (0);
        }

        //Rozpoczêcie oblicznia wariacji z powtórzeniami.
        wariacje_z_powtorzeniami = ObliczWariacjeZPowtorzeniami(wielkosc_zbioru, m);
        printf("\nIlosc wariacji %d elementowych z powtorzeniami zbioru %d elementowego wynosi: %0.0f\n\n", m, wielkosc_zbioru, wariacje_z_powtorzeniami);

        printf("Podaj ilosc generowanych losowych wariacji %d elementowych z powtorzeniami zbioru %d elementowego: ", m, wielkosc_zbioru); //Podanie iloœci losowo generowanych wariacji z powtórzeniami.
        scanf("%d", &k);

        //Sprawdzenie logiki matematycznej
        if (k > wariacje_z_powtorzeniami)
        {
                printf("\nPodales %d losowan waracji z powtorzeniami wiecej niz jest wariacji %d elementowych z powtorzeniami zbioru %d elementowego!\n\n", k, m, wielkosc_zbioru);
                printf("\nNacisnij ENTER");

                getche();
                exit (0);
        }

        //Sprawdzenie ograniczeñ na iloœæ losowañ.
        if (k > 1000)
        {
                printf("\n\n\nPodales ilosc losowan poza zakresem!\n\n");
                printf("\n\nNacisnij ENTER");

                getche();
                exit (0);
        }

        srand(time(0)); //Niepowtarzalnoœæ losowañ.
        //Uwaga! - w przypadku wariacji, czy to z powtórzeniami, czy nie istotna jest kolejnoœæ elementów, czyli np. wariacji {2,1} i wariacja {1,2} s¹ to dwie ró¿ne wariacje.
        GENEROWANIE_LOSOWYCH_WARIACJI_Z_POWTORZENIAMI_W_LICZBIE_k(wielkosc_zbioru, m, k);

        printf("\n\nNacisnij ENTER");

        getche();
        return 0;
}
//---------------------------------------------------------------------------
