//---------------------------------------------------------------------------
#pragma hdrstop
//---------------------------------------------------------------------------
#pragma argsused

#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <time.h>

//Start funkcji obliczaj�cej silnie (permutacje).
float Silnia(int wielkosc_zbioru) //Funkcja obliczaj�ca silnie (permutacje zbioru), w spos�b rekurencyjny.
                                //Opis rekurencji: Najpierw jest wywo�ywana funkcja z wielko�ci� zbioru, nast�pnie jest ona wywo�ywana z co raz mniejsz� wielko�ci� zbioru, a� do jeden.
                                //Nast�pnie od najmniejszej wielko�ci zbioru r�wnej jeden, funkcja cofa si� (obliczaj�c kolejne silnie) do wywo�ania funkcji z wielko�ci� zbioru podan� na pocz�tku.
{
//Wz�r: n * n * n * ... * n = n!
//      1   2   3         n

        if ((wielkosc_zbioru == 1) || (wielkosc_zbioru == 0))
        {
                return (1);
        }
        else
        {
                return ( wielkosc_zbioru * Silnia(wielkosc_zbioru - 1) ); //Wchodzimy w g��b, a� do jeden.
        }
}

//Start funkcji obliczaj�cej wariacje z powt�rzeniami.
float ObliczWariacjeZPowtorzeniami(int wielkosc_zbioru, int m)
{
//Wz�r: n do m (n^m)

        int wynik = wielkosc_zbioru;
        for (int i = 1; i < m; i++)
        {
                wynik = wynik * wielkosc_zbioru;
        }
        return (wynik);
}

void GENEROWANIE_LOSOWYCH_WARIACJI_Z_POWTORZENIAMI_W_LICZBIE_k(int wielkosc_zbioru/*35*/, int wielkosc_podzbioru/*moje 16*/, int k)
//GENEROWANIE k LOSOWYCH WARIACJI wielkosc_podzbioru ELEMENTOWYCH Z POWT�RZENIAMI ZE ZBIORU wielkosc_zbioru ELEMENTOWEGO.
{
     int tmp[20][1000]; //pami�� ju� wylosowanych wariacji.

     int podzbior[20]; //podzbi�r przechowuj�cy wygenerowan� losowo wariacje z powt�rzeniami.
     int i, j, z; //- liczniki p�tli.
     int zmp; //wylosowana pojedy�cza liczba w zakresie od 1 do wielkosc_zbioru
     int powtorka; //zmienna odpowiedzialna za losowanie podzbior, tak d�ugo a� b�dzie to w�a�ciwa z punktu widzenia matematycznego wariacja wielkosc_podzbioru elementowa z powt�rzeniami ze zbioru wielkosc_zbioru elementowego.
     int powtorka2; //powtarzanie generowania wariacji z powt�rzeniami, tak d�ugo a� b�dzie si� powtarza�. Celem jest uzyskanie nie wygenerowanej wcze�niej wariacji z powt�rzeniami.
     int ilosc; //zmienna odpowiedzialna za wylosowanie tylko wielkosc_podzbioru element�w do podzbioru podzbior.
     int przecinek; //czy wypisywa� przecinek.

     //ILOSC LOSOWA� - ALGORYTM GENEROWANIA WARIACJI Z POWT�RZENIAMI.
     for (j = 1; j <= k; j++)
     {
          przecinek = 0;
          ilosc = 0;

          //Losowanie wariacji wielkosc_podzbioru elementowej z powt�rzeniami ze zbioru wielkosc_zbioru elementowego.
          powtorka2 = 0;
          while (powtorka2 == 0)
          {
              //Wylosowanie prawid�owej z punktu widzenia matematyki wariacji z powt�rzeniami.
              while (ilosc < wielkosc_podzbioru)
              {
                 zmp = rand() % wielkosc_zbioru + 1;
                 ilosc = ilosc + 1;
                 podzbior[ilosc-1] = zmp;
              };

              //Sprawdzenie, czy ta wariacja z powt�rzeniami by�a ju� wcze�niej wylosowana i jak tak to powt�rka losowania.
              powtorka2 = 1;
              for (i = 1; i <= k; i++)
              {
                  if (tmp[0][i-1] == 0)
                  {
                     break;
                  }
                  for (z = 1; z <= wielkosc_podzbioru; z++)
                  {
                      if (tmp[z-1][i-1] == podzbior[z-1])
                      {
                          powtorka2 = 0;
                      }
                      else
                      {
                          powtorka2 = 1;
                          break;
                      }
                  }
                  if (powtorka2 == 0)
                  {
                      for (i = 1; i <= wielkosc_podzbioru; i++)
                      {
                         podzbior[i-1] = 0;
                      }
                      ilosc = 0;
                      break;
                  }
              }
          }
          //Koniec losowania wariacji wielkosc_podzbioru elementowej z powt�rzeniami ze zbioru wielkosc_zbioru elementowego.

          printf("\nWariacja z powtorzeniami %d: ", j);

          //Wypisywanie i zapami�tywanie wariacji z powt�rzeniami.
          for (i = 1; i <= wielkosc_podzbioru; i++)
          {
                 printf("%d", podzbior[i-1]);
                 tmp[i-1][j-1] = podzbior[i-1];
                 przecinek = przecinek + 1;
                 if (przecinek < wielkosc_podzbioru)
                 {
                    printf(", ");
                 }
          }
          //Ponowne zerowanie podzbior i przygotowanie do nast�pnego generowania.
          for (i = 1; i <= wielkosc_podzbioru; i++)
          {
                 podzbior[i-1] = 0;
          }
          printf("\n");
     }
}

//START PROGRAMU.
int main(int argc, char* argv[])
{
        int wielkosc_zbioru;
        int m; //m - podzbiory m-elementowe zbioru n-elementowego (wielko�� podzbioru - wariacji z powt�rzeniami).
        int k; //k - ilo�� losowa�.
        float silnia; //wynik silnia (permutacje). //Zastosowa�em liczb� rzeczywist� chocia� wynik permutacji jest zawsze ca�kowity, lecz liczby ca�kowite maj� zbyt ma�y zakres.
        float wariacje_z_powtorzeniami; //wynik wariacje z powt�rzeniami. //Zastosowa�em liczb� rzeczywist� chocia� wynik wariacji jest zawsze ca�kowity, lecz liczby ca�kowite maj� zbyt ma�y zakres.

        printf("Podaj wielkosc zbioru (od 3 do 20): ");
        scanf("%d", &wielkosc_zbioru);

        //Sprawdzenie ogranicze� na wielko�� zbioru.
        if ( (wielkosc_zbioru < 3) || (wielkosc_zbioru > 20) )
        {
                printf("\n\nPodales wielkosc zbioru poza zakresem!\n\n");
                printf("\n\nNacisnij ENTER");

                getche();
                exit (0);
        }

        //Rozpocz�cie oblicznia silni (permutacji).
        silnia = Silnia(wielkosc_zbioru);

        printf("\nPodaj wielkosc podzbioru: "); //Podanie wielko�ci podzbioru.
        scanf("%d", &m);

        if (m > wielkosc_zbioru)
        {
                printf("\nPodales wielkosc podzbioru wieksza niz wielkosc zbioru!\n\n");
                printf("\nNacisnij ENTER");

                getche();
                exit (0);
        }

        //Rozpocz�cie oblicznia wariacji z powt�rzeniami.
        wariacje_z_powtorzeniami = ObliczWariacjeZPowtorzeniami(wielkosc_zbioru, m);
        printf("\nIlosc wariacji %d elementowych z powtorzeniami zbioru %d elementowego wynosi: %0.0f\n\n", m, wielkosc_zbioru, wariacje_z_powtorzeniami);

        printf("Podaj ilosc generowanych losowych wariacji %d elementowych z powtorzeniami zbioru %d elementowego: ", m, wielkosc_zbioru); //Podanie ilo�ci losowo generowanych wariacji z powt�rzeniami.
        scanf("%d", &k);

        //Sprawdzenie logiki matematycznej
        if (k > wariacje_z_powtorzeniami)
        {
                printf("\nPodales %d losowan waracji z powtorzeniami wiecej niz jest wariacji %d elementowych z powtorzeniami zbioru %d elementowego!\n\n", k, m, wielkosc_zbioru);
                printf("\nNacisnij ENTER");

                getche();
                exit (0);
        }

        //Sprawdzenie ogranicze� na ilo�� losowa�.
        if (k > 1000)
        {
                printf("\n\n\nPodales ilosc losowan poza zakresem!\n\n");
                printf("\n\nNacisnij ENTER");

                getche();
                exit (0);
        }

        srand(time(0)); //Niepowtarzalno�� losowa�.
        //Uwaga! - w przypadku wariacji, czy to z powt�rzeniami, czy nie istotna jest kolejno�� element�w, czyli np. wariacji {2,1} i wariacja {1,2} s� to dwie r�ne wariacje.
        GENEROWANIE_LOSOWYCH_WARIACJI_Z_POWTORZENIAMI_W_LICZBIE_k(wielkosc_zbioru, m, k);

        printf("\n\nNacisnij ENTER");

        getche();
        return 0;
}
//---------------------------------------------------------------------------
