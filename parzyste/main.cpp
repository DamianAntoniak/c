#include <iostream>
#include <conio.h>
#include "windows.h"

using namespace std;

int main()
{
    system("chcp 1250");
    system("cls");
    int wax;
    do
    {
        cout << "Podaj liczbe" << endl;
        cin >> wax;

        if(wax / 2 && !(wax % 2)) cout << "Liczba " << wax << " jest parzysta i ";
        else cout << "Liczba " << wax << " jest nie parzysta i ";

        if(wax >= 1) cout << "jest wieksza niz 0 oraz ";
        else if(wax == 0) cout << "jest rowna 0 oraz ";
        else if(wax < 0) cout << "jest mniejsza niz 0 oraz ";

        if(!(wax % 7)) cout << "jest podzielna przez 7!\n";
        else cout << "NIE jest podzielna przez 7!\n";

        cout << "By powrocic do poczatku programu nacisnij Y." << endl;
    }
    while(getch() == 'y');
    return 0;
}
