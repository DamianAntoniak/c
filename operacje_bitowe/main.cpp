#include <iostream>//by Wax

using namespace std;

int main()
{
    int x, y;
    cout << "wprowadz liczbe od 0 do 15 dla x \n";
    cin >> x;
    cout << "Wprowadz liczbe od 0 do 15 dla y \n";
    cin >> y;
    cout << "Bitowa alternatywa: " << (x | y) << endl;
    cout << "Bitowa koniunkcja: " << (x & y) << endl;
    cout << "Bitowa roznica symetryczna: " << (x ^ y) << endl;
    cout << "Bitowe przesuniecie w lewo: " << (x << y) << endl;
    cout << "Bitowe przesuuniecie w prawo: " << (x >> y) << endl;
    cout << "Negacja bitowa x: " << ~(x) << endl;
    return 0;
}
