/*
    by Wax aka Takisobiekoles @ 08.09.2013r
        Skype: fty997
        e-mail: damianapl17pl@gmail.com
*/
#include <iostream>

#define Wax_Star 1
#define Wax_Exclamation_Mark 2

using namespace std;

void Symbol(char);
void Chrismas_Tree(int);

char inny;
int znak;

int main()
{
    char t;
    int w;
    cout << "Wybierz znak, z ktorego ma by wyswietlona choinka:" << endl << "Dostpne: *, ! - lub uzyj wlasnego :)" << endl;
    cin >> t;
    cout << endl;

    Symbol(t);

    cout << "Podaj wysokosc choinki:" << endl;
    cin >> w;
    cout << endl << endl;

    Chrismas_Tree(w);
    return 0;
}

void Symbol(char a)
{
    switch(a)
    {
        case '*':
        {
            znak = Wax_Star;
            break;
        }
        case '!':
        {
            znak = Wax_Exclamation_Mark;
            break;
        }
        default:
        {
            inny = a;
            break;
        }
    }
}

void Chrismas_Tree(int how)
{
    int q, k;
	int w, a, x;
	char y;

	for(w = 0; w < how; w++)
    {
		a = how;
		x = 1;
		for(q = 0; q < w + 2; q++)
		{
			for(k = 0; k < a; k++) cout << " ";
			for(k = 0; k < x; k++)
			{
                if(znak == Wax_Star) cout << "*";
                else if(znak == Wax_Exclamation_Mark) cout << "!";
                else cout << inny;
			}
			cout << endl;
			x += 2;
			a--;
		}
	}
}
