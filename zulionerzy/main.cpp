/*
    by Wax aka Takisobiekoles @ 20.06.2013r
        Skype: fty997
        e-mail: damianapl17pl@gmail.com
*/
#include <iostream>
#include <fstream>
#include "Windows.h"

#define VERSION "0.1"
#define AUTHOR "Wax"

using namespace std;

void Game();
void NumberOfLines();

int w = 0;
char los;
int main()
{
    int a = 1;
    system("chcp 1250");
    system("cls");

    cout << "Zulionerzy v"VERSION" by "AUTHOR".\n";

    NumberOfLines();

    while(a == 1)
    {
        cout << "\n\nNacisnij 1 by rozpoczą gre.\n";
        cin >> a;

        switch(a)
        {
            case 0:
            {
                system("EXIT");
                break;
            }
            case 1:
            {
                char p;
                cout << "Gra rozpoczęta\n";
                do
                {
                    Game();
                    cin >> p;
                }
                while(p == los);
                cout << "Niestety, odpowiedź " << p << " jest bledna. Poprawna to " << los << endl;
                break;
            }
        }
    }
    return 0;
}



void Game()
{
    fstream file;
    bool good();
    file.open("quiz.txt");
    string zawartosc;
    int t = 0;
    string test;
    int pytanie = ((rand() % (w - 1)) + 1);

    if(file.good() == true)
    {
        while(!file.eof())
        {
            getline(file, test);
            if(!test.size() || (test[0] >= '0' && test[0] <= '9'))
            {
                t++;
                if(pytanie == t)
                {
                    getline(file, zawartosc);
                }
            }
        }

        string pyt;
        string odp1;
        string odp2;
        string odp3;
        string odp4;

        int wax[5];

        for(int q = 0; q < 5; q++) wax[q] = 0;

        for(int i = 0; i < zawartosc.size(); i++)
        {
            if(zawartosc[i] == '=')
            {
                if(wax[0] < 1)wax[0] = i + 1;
                else if(wax[1] < 1) wax[1] = i + 1;
                else if(wax[2] < 1) wax[2] = i + 1;
                else if(wax[3] < 1) wax[3] = i + 1;
                else if(wax[4] < 1) wax[4] = i + 1;
            }
        }
        pyt = zawartosc.substr(wax[0], wax[1] - wax[0] - 1);
        odp1 = zawartosc.substr(wax[1], wax[2] - wax[1] - 1);
        odp2 = zawartosc.substr(wax[2], wax[3] - wax[2] - 1);
        odp3 = zawartosc.substr(wax[3], wax[4] - wax[3] - 1);
        odp4 = zawartosc.substr(wax[4], zawartosc.size());


        cout << "\nPytanie: " << pyt << endl;
        cout << endl << "\nOdpowiedzi\n";

        int l = (rand() % 4) + 1;
        switch(l)
        {
            case 1:
            {
                cout << endl << "A) " << odp1 << endl << "B) "<< odp2 << endl << "C) " << odp3 << endl << "D) " << odp4 << endl;
                los = 'D';
                break;
            }
            case 2:
            {
                cout << endl << "A) " << odp4 << endl << "B) "<< odp3 << endl << "C) " << odp2 << endl << "D) " << odp1 << endl;
                los = 'A';
                break;
            }
            case 3:
            {
                cout << endl << "A) " << odp3 << endl << "B) "<< odp4 << endl << "C) " << odp1 << endl << "D) " << odp2 << endl;
                los = 'B';
                break;
            }
            case 4:
            {
                cout << endl << "A) " << odp2 << endl << "B) "<< odp1 << endl << "C) " << odp4 << endl << "D) " << odp3 << endl;
                los = 'C';
                break;
            }
        }

    }
    else cout << "Brak dostępu do pliku." << endl;
}

void NumberOfLines()
{
    fstream filee;
    bool good();
    filee.open("quiz.txt");
    string st;

    while(!filee.eof())
    {
        w++;
        getline(filee, st);
    }
    filee.close();
    w-=1;
    cout << "Wczytano: " << w << " pytań";
}
